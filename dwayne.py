from cli import User, Dwayne

import argparse
import sys
import yaml
import json
import os


def parse_args(args):
    parser = argparse.ArgumentParser(usage='dwayne.py [-h] [-l] config component command [name [params [params ...]]')
    parser.add_argument("config", type=str, help="path to config file")
    parser.add_argument("component", type=str,
                        #choices=['provider', 'profile', 'vm', 'puppet'],
                        help="dwayne component to act on")

    parser.add_argument("command", type=str,
                        #choices=['create', 'destroy', 'list', 'configure'],
                        help="the action/method to run for the given component")
    parser.add_argument("name", type=str, default="", nargs='?',
                        help="the name/identier of a specific component instance")
    parser.add_argument("params", type=str, nargs='*',
                        help="params passed along to the specified component command")
    parser.add_argument("-l", "--load", dest="load", action="store_true",
                        help="automatically load/update any profiles/providers listed in the cfg")


    #vm_group = parser.add_argument_group('vm component options')
    #vm_group.add_argument("--cpus", dest="num_cpus", type=int,
    #                       help="number of cpus to assign to a created VM")
    #vm_group.add_argument("--memory", dest="memory", type=str,
    #                       help="the amount of memory to assign to the vm (4GB, 1024MB, etc)")
    return parser.parse_args()


def process_args(args):
    args = parse_args(args)
    config = args.config
    
    conf_data = load_config(config)
    conf_data.update(vars(args))

    component = args.component
    command = args.command
    name = args.name

    converted_args = []
    if component == 'vm':
        if not name:
            name = os.path.basename(config).split('.')[0]
        #if not args.params: #len(args) < 5:
        else:
            profile = conf_data['profile']
            if command == 'create':
                converted_args = [profile['name']]
            elif command == 'destroy':
                converted_args = [profile['provider']]

    for param in args.params:
        try:
            param = json.loads(param)
        except:
            pass
        converted_args.append(param)
    return conf_data, component, command, name, converted_args, args


def load_config(path):
    return yaml.load(open(path))


def main():
    conf, component, command, name, cmd_args, args = process_args(sys.argv[1:])

    user = User(conf['username'], conf['team'])
    print('Authenticating user {}'.format(conf['username']))
    user.authenticate(conf['auth_url'], conf['password'])

    dwayne = Dwayne(conf['url'], user)

    if args.load:
        if conf.get('provider'):
            pname = conf['provider']['name']
            del(conf['provider']['name'])
            print('Loading/updating provider: {}'.format(pname))
            dwayne.provider.create(pname, conf['provider'])
        if conf.get('profile'):
            pname = conf['profile']['name']
            del(conf['profile']['name'])
            print('Loading/updating profile: {}'.format(pname))
            dwayne.profile.create(pname, conf['profile'])

    result = dwayne(component, command, name, *cmd_args)
    print(result)


if __name__ == '__main__':
    main()
