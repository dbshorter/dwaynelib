import os
import requests
import json
import threading
from time import sleep

class User(object):
    def __init__(self, username, team):
        self.username = username
        self.team = team
        self.auth = None

    def authenticate(self, host, password):
        url = os.path.join(host, 'api/auth/token')
        resp = requests.post(url, auth=(self.username, password))
        if resp.ok:
            self.auth = resp.json()
            return self.auth
        else:
            # raise auth error
            pass

class DwayneCall(object):
    UNCHANGED = 200
    CREATED = 201
    RUNNING = 202
    MISSING = 404
    ERROR = 500

    SUCCESS = [UNCHANGED, CREATED, RUNNING]
    FAIL = [MISSING, ERROR]

    URL = ''

    def __init__(self, host, user):
        self.host = host
        self.user = user
        self.endpoint = os.path.join(host, self.URL)
        self._resp = False
        self.headers = {'accept': 'application/json', 'Content-Type': 'application/json'}
        if self.user.auth:
            self.headers['authorization'] = '{} {}'.format(self.user.auth['token_type'], self.user.auth['access_token'])

    def run(self):
        pass

    def response(self):
        if self.success():
            #print(self._resp.json())
            return self._resp.json()
        else:
            # raise exception
            print('Response Error')
            print(self._resp)
            print(self._resp.text)
            return self._resp
            #return None

    def _url(self, path=''):
        if path:
            return os.path.join(self.endpoint, path)
        else:
            return self.endpoint

    def get(self, path, params):
        #print(self._url(path))
        self._resp = requests.get(self._url(path), headers=self.headers, params=params)
        return self.response()

    def success(self):
        return self._resp and self._resp.status_code in self.SUCCESS

    def post(self, path='', params=None, body=None):
        #print(self._url(path))
        #if not params: params = {}
        #if not body: body = {}
        self._resp = requests.post(self._url(path), headers=self.headers, params=params, data=json.dumps(body))
        return self.response()

    def delete(self, path='', params=None, body=None):
        self._resp = requests.delete(self._url(path), headers=self.headers, params=params, data=json.dumps(body))
        return self.response()

    def _poll_jobs(self):
        results = {}
        thread_pool = []
        if self.success() and 'jid' in self._resp.json():
            jids = self._resp.json()['jid']
            if not isinstance(jids, list):
                jids = [jids]
            for jid in jids:
                job = threading.Thread(target=self._wait_for_job, args=(jid, results))
                thread_pool.append(job)
                job.start()

            [job.join() for job in thread_pool]
            return [result.ok and result.json() or result for result in results.values()]
        else:
            return self.response()

    def _wait_for_job(self, jid, results):
        url = os.path.join(self.host, 'api/job/status/{}'.format(jid))
        while True:
            #print(url)
            resp = requests.get(url, headers=self.headers)
            #print(resp)
            results[jid] = resp
            if results[jid].status_code != self.RUNNING:
                break
            sleep(5)

    def put(self, path='', params=None, body=None):
        self._resp = requests.put(self._url(path), headers=self.headers, params=params, data=json.dumps(body))
        return self.response()


class DwayneVM(DwayneCall):
    URL = 'api/vm'

    def _params(self, profile=None, provider=None, count=1):
        params = {'team': self.user.team}
        if provider:
            params['provider'] = provider
        if profile:
            params['profile'] = profile
        if count > 1:
            params['start'] = 1
            params['end'] = count
        return params

    def create(self, name, profile, count=1):
        params = self._params(profile=profile, count=count)
        self.post(name, params=params)
        return self._poll_jobs()

    def destroy(self, name, provider, count=1):
        params = self._params(provider=provider, count=count)
        self.delete(name, params=params)
        return self._poll_jobs()

    def list(self, name):
        self.get(name, params=self._params())
        return self._poll_jobs()


class DwayneProfile(DwayneCall):
    URL = 'api/template/profile'

    def list(self, name=''):
        return self.get(name, params={'team': self.user.team})

    def create(self, name, body):
        return self.put(name, params={'team': self.user.team}, body=body)


class DwayneProvider(DwayneCall):
    URL = 'api/template/provider'

    def list(self, name=''):
        return self.get(name, params={'team': self.user.team})

    def create(self, name, body):
        return self.put(name, params={'team': self.user.team}, body=body)


class DwaynePuppet(DwayneCall):
    URL = 'api/bootstrap/puppet'

    def configure(self, hostname, body):
        body['instance_name'] = hostname
        body['user_team'] = self.user.team
        self.post(body=body)
        return self._poll_jobs()


class Dwayne(object):
    def __init__(self, host, user):
        self.provider = DwayneProvider(host, user)
        self.vm = DwayneVM(host, user)
        self.profile = DwayneProfile(host, user)
        self.puppet = DwaynePuppet(host, user)

    def __call__(self, component, command, name, *args, **kwargs):
        call_obj = getattr(self, component)
        call_action = getattr(call_obj, command)
        return call_action(name, *args, **kwargs)

